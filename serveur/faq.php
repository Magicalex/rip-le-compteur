<?php 
include("preprocess.php");
include("head.inc");
?>

<main>
    
    <section class="centered">
        <h2>FAQ</h2>
        <p>Informations et méthode</p>
    </section>

    <section>
        <h3>D'où proviennent les données ?</h3>
        <p>Les signatures sont détectées lorsque des contributeurs visitent les pages du site web du Ministère de l'Intérieur dans la section "<a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8" target="_blank" rel="noopener noreferrer">Consultation des soutiens déposés</a>". L'extension de navigateur installée sur leur navigateur détecte les signatures, les comptent et les envoient vers ce site web qui centralise toutes les contributions. Les contributeurs doivent visiter les dernières pages de chaque digramme (couple de lettres) pour obtenir le nombre précis de signatures.</p>
    </section>

    <section>
        <h3>Comment devient-on contributeur ? Qui sont les contributeurs ?</h3>
        <p>Tout le monde peut être contributeur après avoir installé une extension de navigateur <a href="https://dav.li/rip-le-compteur/rip_le_compteur-1.5-fx.xpi">disponible ici</a> (uniquement pour Firefox). Visitez la page "<a href="contribuer.php">Contribuer</a>" pour savoir quel digramme est à mettre à jour !</p>
    </section>

    <section>
        <h3>Comment est calculé le nombre de signatures ? Pourquoi est-ce une estimation ?</h3>
        <p>Le nombre total de signatures est calculé en additionnant les signatures comptées sur chaque digramme (AA, AB, AC, AD, etc.). Ensuite, une estimation est faite sur les signatures non affichées.</p>
        <p>Nous avons remarqué le 25 juin une différence entre le comptage via la méthode simplifiée mais <a href="https://www.liberation.fr/checknews/2019/06/26/referendum-adp-l-interieur-casse-le-compteur_1736096" target="_blank" rel="noopener noreferrer">bloquée par le Ministère ce même jour</a>, et le chiffre de signatures comptées collaborativement. Nous avions compté 373 772 signatures alors que le site du Ministère affichait une liste de 378 477 signatures juste avant que cette liste ne soit rendue inaccessible. Cette différence de 4705 signatures peut être due à une erreur de comptage de notre part (mais cela est peu probable) ou à des erreurs du côté du Ministère. En effet, nous avons remarqué que leur site web n'affichait pas les signataires qui ont un nom d'un caractère, les signataires qui ont une lettre avec accent dans l'une des deux premières lettres de leur nom et lorsqu'il y avait des erreurs typographiques (comme une espace avant la première lettre). Par conséquent, au nombre de signatures détectées, il est ajouté une estimation du nombre de signatures non-affichées calculée comme ceci :<br/>
        <strong>total_avec_estimation = signature_détectées_aujourdhui * ( 1 + ( signatures_méthode_une_page_25juin - signatures_méthode_collab_25juin ) / signatures_méthode_collab_25juin )</strong><br/>
        <i>Avec </i>signatures_méthode_une_page_25juin<i>, le nombre total de signatures calculé via l'ancienne méthode au 25 juin, soit la valeur constante : 378477.</i><br/>
        <i>Avec </i>signatures_méthode_collab_25juin<i>, le nombre total de signatures détectées collaborativement au 25 juin, soit la valeur constante : 373772.</i><br/>
        <i>Avec </i>signatures_détectées_aujourdhui<i>, les signatures détectées au moment du calcul.</i><br/>
        <i>Donc, avec les données d'aujourd'hui :</i><br/>
        <strong>total_avec_estimation = <?php echo($total_digramme);  ?> * ( 1 + ( 378477 - 373772 ) / 373772 )</strong><br/>
        <i>Soit :</i><br/>
        <strong>total_avec_estimation = <?php echo($total_digramme);  ?> * ( 1 + 4705 / 373772 )</strong><br/>
        <i>Soit (avec arrondi de la division à 4 décimales) :</i><br/>
        <strong>total_avec_estimation = <?php echo($total_digramme);  ?> * ( 1 + 0.0126 )</strong><br/>
        <i>Et donc :</i><br/>
        <strong>total_avec_estimation = <?php echo($total_digramme+$estimation_signatures_nonaffichees);  ?></strong><br/>
        <i>Chiffre qui est au final arrondi au millier inférieur, pour ne pas surévaluer le résulat. Comme nous n'avons la différence de signatures entre la méthode une page (bloquée par le Ministère) et la méthode collaborative que pour un seul jour (le 25 juin), cette statistique reste moyennement précise.</i></p>
    </section>

</main>
<?php include("footer.inc"); ?>
</body>
</html>