<?php include("preprocess.php");

include("head.inc");

?>

<main>
    
    <section class="centered">
        <h2><img src="twemoji/poing.png" alt="emoji poing" /> Contribuer</h2>
        <p>Pour information, sur ce compteur, <?php echo($totalContrib); ?> contributions ont été enregistrées. Merci pour votre aide !</p>
        <p>Avant de contribuer, soyez sûr d'avoir installé l'extension :</p>
        <p><a href="rip_le_compteur-1.5-fx.xpi" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox</a></p>
        <p>Version 1.5 (mise à jour le 21/06/19 à 18h15)</p>
    </section>

    <section>
        <p>
            <?php
            if(sizeof($bdd_maj_priorite)==0){
                if($total_a_maj!=0){
                    echo("Il n'y a plus aucun digramme à mettre à jour en priorité. Bravo ! En revanche, il reste toujours ".$total_a_maj." digrammes peu populaires...");
                }else{
                    echo("Il n'y a plus aucun digramme à mettre à jour. Bravo !");
                }
            }else{
                echo('Il reste <span class="badge">'.sizeof($bdd_maj_priorite).'</span> digrammes à mettre à jour en priorité !<br/>');
                if(sizeof($bdd_maj_priorite)>=5){
                    echo("Voici 5 digrammes pour vous : ");
                    shuffle($bdd_maj_priorite);
                    $bdd_maj_priorite=array_slice($bdd_maj_priorite, 0, 5);
                }else{
                    echo("Hop ! Voici les derniers digrammes : ");
                }
                foreach ($bdd_maj_priorite as $s) {
                    echo('<a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank">'.$s.'</a> ');
                }
                echo('<button id="ouvrirTout" class="btn">Tout ouvrir <i class="fa fa-arrow-right" aria-hidden="true"></i></button>');
            }
            ?>
        </p>
        <div id="tabs">
            <ul>
                <li><a href="#tabs-1">Alphabétique</a></li>
                <li><a href="#tabs-2">Popularité</a></li>
                <li><a href="#tabs-3">A mettre à jour <span class="badge"><?php echo($total_a_maj); ?></span></a></li>
                <li><a href="#tabs-4">A préciser <span class="badge"><?php echo($preciser); ?></span></a></li>
            </ul>
            <div id="tabs-1">
                <table class="stats">
                    <?php
                    foreach ($bdd_best as $s => $donnees) {
                        echo('<tr>');
                        echo('<th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$donnees["nb_pages"].'" target="_blank">'.$s.'</a></th>');
                        echo('<th class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</th>');
                        echo('</tr>'."\n");
                    }
                    ?>
                </table>
            </div>
            <div id="tabs-2">
                <table class="stats">
                    <?php
                    echo($table_popularite);
                    ?>
                </table>
            </div>
            <div id="tabs-3">
                <label for="slider">Signatures :</label>
                <input type="text" id="slider" readonly style="border:0; font-weight:bold;">
                <div id="slider-range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                    <div class="ui-slider-range ui-corner-all ui-widget-header"></div>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                    <span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default"></span>
                </div>
                <table class="stats" style="margin-top:20px;">
                    <?php
                    echo($table_maj);
                    ?>
                </table>
            </div>
            <div id="tabs-4">
                <table class="stats">
                <?php
                echo($table_preciser);
                ?>
                </table>
            </div>
        </div>
    </section>

</main>
<?php include("footer.inc"); ?>
    <script src="//dav.li/jquery/3.1.1.min.js"></script>
    <script src="//dav.li/jquery/ui/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $(document).tooltip();
            $("#tabs").tabs();
            
            $("#slider-range").slider({
                range: true,
                min: 0,
                max: <?php echo($maximum); ?>,
                values: [0, <?php echo($maximum); ?>],
                slide: function(event, ui) {
                    $("#slider").val(ui.values[0] + " - " + ui.values[1]);
                    $("#slider~.stats .badge").each(function() {
                        if (parseInt($(this).text()) < ui.values[0] || parseInt($(this).text()) > ui.values[1]) {
                            $(this).parents("tr").hide();
                        } else {
                            $(this).parents("tr").show();
                        }
                    })
                }
            });
            $("#slider").val($("#slider-range").slider("values", 0) + " - " + $("#slider-range").slider("values", 1));
        
            $("#ouvrirTout").click(function(){
                $(this).siblings("a").each(function(){
                    window.open($(this).attr("href"));
                });
            })

        });
    </script>
</body>
</html>