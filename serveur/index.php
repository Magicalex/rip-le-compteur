<?php include("preprocess.php");

$now=new DateTime("now");

include("head.inc"); ?>

    <a id="notification" class="btn" href="contribuer.php" title="Aider à mettre à jour le compteur">
        <?php if(sizeof($bdd_maj_priorite)!=0){ echo('<span class="badge">'.sizeof($bdd_maj_priorite).'</span>'); } ?>
        <i class="fa fa-refresh" aria-hidden="true"></i>
    </a>
    <main>
        <section class="centered">
            <h1>
                <span><?php echo(number_format($total, 0, ',', ' ')); ?></span>
            </h1>
            <p>Signatures détectées avec estimation* sur la <a href="https://www.referendum.interieur.gouv.fr/" target="_blank" rel="noopener noreferrer">proposition de loi référendaire</a> visant à affirmer le caractère de service public national de l'exploitation des aérodromes de Paris sur 4 717 396 nécessaires, soit
                <?php echo($pourcentage); ?>%.
                <div id="progressbar"><span style="width:<?php echo($pourcentage); ?>%"><span><?php echo($pourcentage); ?>%</span> </span>
                </div>
            </p>
            <p style="margin: 30px auto"><a href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fdav.li/rip-le-compteur/&ref_src=twsrc%5Etfw&text=<?php echo($total); ?>%20signatures%20pour%20le%20%23ReferendumADP%20selon%20le%20compteur%20collaboratif&tw_p=tweetbutton&url=https%3A%2F%2Fdav.li/rip-le-compteur/" target="_blank">Partager ce compteur collaboratif #ReferendumADP <i class="fa fa-twitter" aria-hidden="true"></i></a></p>
            
            <p><i>*Données collectées collaborativement. Chiffre estimé à partir de <?php echo($total_digramme); ?> signatures détectées et un ajout de <?php echo($estimation_signatures_nonaffichees); ?> signatures estimées non affichées sur le site web du Ministère de l'Intérieur. <a href="faq.php">En savoir plus sur la méthode et le calcul</a>. Des erreurs peuvent altérer la véracité de ce chiffre. Toutes les données sont <a href="data.txt" target="_blank">en libre accès</a>, notament <a href="api.php?endpoint=cache" target="_blank">via l'API</a>, pour faire vos propres calculs et vérifications.</i></p>
        
            <p class="alert"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> Le Ministère de l'Intérieur a mis à jour son site web et a rendu la méthode de calcul simplifiée impossible à utiliser. Cela complique grandement le comptage des signatures. <a href="https://twitter.com/DavidLibeau/status/1143507757070524417" target="_blank" rel="noopener noreferrer">Si d'un point de vue technique, c'était prévisible, en revanche, d'un point de vue démocratique, c'est une honte.</a> Ce compteur est repassé sur l'ancienne méthode de comptage qui induit la visite manuelle de beaucoup de pages (tous les digrammes). Le travail est donc plus important : n'hésitez pas à aider en contribuant ! La <a href="contribuer.php">liste présente sur cette page</a> permet de voir les digrammes à mettre à jour.</p>
        </section>
        <section>
            <h2><img src="twemoji/poing.png" alt="emoji poing" /> Aider à améliorer cette statistique en installant un module sur votre navigateur</h2>
            <p>Le site du ministère de l'intérieur ne permet pas d'obtenir le nombre de signatures total. Pour avoir ce chiffre, il faudrait que chacun compte les signatures affichées sur le site web. Heureusement, un script permet de le faire à votre place et de transmettre ce résultat à ce site web. L'objectif est de centraliser le comptage fait par les citoyens qui ont installé l'extension sur leur navigateur et ainsi d'avoir le nombre de signataires.</p>
            <p>Après avoir installé l'extension, il vous faudra visiter les pages listant les signatures sur le site du ministère et résoudre les captcha. L'extension détectera automatiquement les signatures. Une liste des digrammes (couples de lettres) à mettre à jour est disponible en bas de ce site web. Merci !</p>
        </section>
        <section class="centered">
            <div class="content">
                <div class="demi">
                    <h3>Etape 1</h3>
                    <p><a href="rip_le_compteur-1.5-fx.xpi" class="btn"><i class="fa fa-download" aria-hidden="true"></i> Installer l'extension sur Firefox</a></p>
                    <p>Version 1.5 (mise à jour le 21/06/19 à 18h15)</p>
                </div><div class="demi">
                    <h3>Etape 2</h3>
                    <p><a href="contribuer.php" class="btn">Mettre à jour des digrammes</a></p>
                </div>
            </div>
        </section>
        <section>
            <h2><img src="twemoji/ampoule.png" alt="emoji ampoule" /> Avertissement et conditions d'utilisation</h2>
            <p>L'auteur de ce site web et de l'extension ne peut être tenu responsable d'une mauvaise utilisation ou d'une utilisation malicieuse des outils ou données délivrés par ce site web.</p>
            <p>Les données de ce site web sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>, <a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank" rel="noopener noreferrer">code source</a>) et sont soumis à leur licence respective.</p>
            <p>Les utilisateurs de l'extension de navigateur sont contributeurs et publient les données qu'ils récoltent sous licence Creative Commons Attribution.</p>
            <p>L'auteur de ce site web et de l'extension ne fournit aucune garantie quant à la validité des données fournies par ce site web.</p>
            <p>En utilisant ce site web et en téléchargement l'extension pour navigateur, les utilisateurs donnent leur approbation concernant ces conditions d'utilsation et leur consentement à l'utilisation et la transmission des données nécessaires au bon fonctionnement de ce site web et de l'extension pour navigateur.</p>
            <p>Ce site web et l'extension de navigateur peuvent enregistrer les données de connexion de l'utilisateur et notamment son adresse IP. Ce site web utilise des cookies nécessaires à son fonctionnement. Aucune donnée n'est vendue. Certaines données sont en libre accès (<a href="data.txt" target="_blank">base de données compteur</a>, <a href="error.txt" target="_blank">base de données erreur</a>). L'adresse IP de l'utilisateur est anonymisée (pseudonymisation avec un algorithme sha256 avec ajout d'une chaine de caractères secrete) avant d'être sauvegardée.</p>
            <p>Avant d'utiliser l'extension de navigateur, l'utilisateur doit s'informer de son fonctionnement en consultant le code source en libre accès (<a href="https://framagit.org/DavidLibeau/rip-le-compteur" target="_blank">à cette adresse</a>) ou en demandant des informations à l'auteur de l'extension, si l'utilisateur a des questions.</p>
            <p>L'utilisateur peut à tout moment exercer ses droits auprès de l'auteur du site web et de l'extension de navigateur. L'auteur du site web et de l'extension de navigateur aura 30 jours pour répondre et exercer les droits de l'utilisateur.</p>
            <p>L'auteur de ce site web et de l'extension de navigateur peut être contacté <a href="https://davidlibeau.fr/Contact" target="_blank" rel="noopener noreferrer">à cette adresse</a>.</p>
        </section>
        <section>
            <h2><img src="twemoji/stats.png" alt="emoji statistiques"/> Statistiques supplémentaires</h2>
            <h3>Estimation</h3>
            <p>Si on réunit autant de signatures que depuis le début, il faudrait <strong><?php echo(round(DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 01:00:00")->diff($now)->format("%a")/$total*4717396)); ?> jours</strong> pour obtenir les 4 717 396 signatures nécessaires.</br/>Si on garde la même augmentation de signatures que lors de ces 3 derniers jours, il faudrait <strong><?php $augmentation=0; for($s=sizeof($stats)-1;$s>sizeof($stats)-4;$s--){ $augmentation+=$stats[$s]["total"]-$stats[$s-1]["total"]; } echo(round(4717396/($augmentation/3))); ?> jours</strong> pour obtenir les 4 717 396 signatures nécessaires.</p>
            <h3>Historique</h3>
            <table id="historique" class="content">
                <thead>
                    <tr>
                        <td>Date</td>
                        <td>Total</td>
                        <td>Augmentation</td>
                        <td>Pourcentage</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        foreach($stats as $d => $stat){
                            echo('<tr><td>'.explode(" ",$stat["date"])[0].'</td><td>'.number_format($stat["total"], 0, ',', '&nbsp;').'</td><td>+'.number_format($stat["total"]-$stats[$d-1]["total"], 0, ',', '&nbsp;').'</td><td>'.$stat["pourcentage"].'%</td></tr>');
                        }
                        echo('<tr><td>Maintenant</td><td>'.number_format($total, 0, ',', '&nbsp;').'</td><td>+'.number_format($total-$stats[sizeof($stats)-1]["total"], 0, ',', '&nbsp;').'</td><td>'.$pourcentage.'%</td></tr>');
                    ?>
                </tbody>
            </table>
            <?php 
                $dayDistance=300/sizeof($stats);
                $signatureDistance=140/$total;
                $svgCourbe="";
                $svgPoints="";
                foreach($stats as $d => $stat){
                    $x=round(($d+1)*$dayDistance)-20;
                    $y=200-round($stat["total"]*$signatureDistance);
                    $svgCourbe.=" L ".$x." ".$y;
                    $svgPoints.='<circle title="'.explode(" ",$stat["date"])[0].' : '.number_format($stat["total"], 0, ',', ' ').' signatures" data-total="'.$stat["total"].'" data-date="'.explode(" ",$stat["date"])[0].'" cx="'.$x.'" cy="'.$y.'" r="3"/>';
                }
            ?>
            <p>
                <svg version="1.1" id="graph" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 350 210" style="enable-background:new 0 0 378.9 268.1;" xml:space="preserve">
                <style type="text/css">
                    .st0{fill:#687680;}
                    .st1{fill:#64ABDE;}
                    .st2{fill:#CCD6DD;}
                    .st3{fill:#9AAAB4;}
                </style>
                <g id="courbe">
                    <path d="<?php echo("M 10 200".$svgCourbe." L 310 52 M 310 200 Z"); ?>" stroke="#3c3c3c" stroke-width="2" fill="none"/>
                    <circle title="Début des données" data-total="0" data-date="17/06/19" cx="10" cy="200" r="3"></circle>
                    <?php echo($svgPoints); ?>
                </g>
                <g id="avion">
                    <circle class="st0" cx="318.2" cy="57" r="1.5"/>
                    <circle class="st0" cx="327.7" cy="53.4" r="1.5"/>
                    <path class="st1" d="M315.5,51.7c1,1.1,8-1,5.7-2.3c-7.1-4.6-12.9-4.5-11.2-2.8C312,48.4,313.4,49.8,315.5,51.7z"/>
                    <path class="st2" d="M334.2,47.9c0.4,1.9-3.8,3.9-5.7,4.3l-17.1,3.7c-1.9,0.4-9.2,0.4-9.6-1.5 c-0.4-2,4.2-4.5,6.1-4.9l19.1-4.1C328.9,45,333.8,46,334.2,47.9z"/>
                    <path class="st0" d="M330,45.5c-0.4,0.4-1,0.6-1.4,0.9c-0.5,0.4-0.4,1.8,0.8,1.4c1-0.4,1.9-1.2,2.4-1.8 C331.3,45.8,330.7,45.6,330,45.5z"/>
                    <path class="st1" d="M301.8,54c-0.2-0.8-1.3-5.9-1.5-6.7c-0.5-2.5,3.6,0.7,5.8,2.8C304.2,51.1,302.6,52.1,301.8,54z M302.6,54.2c0.9-0.8,4-1.4,2.4,1c-2.3,3.6-4.5,4-4.1,2.9C301.4,56.8,302,55.5,302.6,54.2z M314.8,53.5c0.5-1.4,7.7-2.4,6.1-0.3 c-4.6,7.1-9.9,9.4-9,7.2C313,57.9,313.6,56.1,314.8,53.5z"/>
                    <circle class="st3" cx="326.1" cy="48.6" r="1"/>
                    <circle class="st3" cx="323.2" cy="49.2" r="1"/>
                    <circle class="st3" cx="320.3" cy="49.9" r="1"/>
                    <circle class="st3" cx="317.3" cy="50.5" r="1"/>
                    <circle class="st3" cx="314.4" cy="51.2" r="1"/>
                    <circle class="st3" cx="311.5" cy="51.8" r="1"/>
                </g>
                </svg>
            </p>

            <p class="centered" style="margin-top:100px"><a href="faq.php" class="btn">En savoir plus sur ce compteur...</a> <a href="contribuer.php" class="btn">Contribuer !</a></p>

        </section>

    </main>
    <?php include("footer.inc"); ?>
    <script src="//dav.li/jquery/3.1.1.min.js"></script>
    <script src="//dav.li/jquery/ui/jquery-ui.min.js"></script>
    <script>
        $(function() {
            $(document).tooltip();
            
            $("footer").css({
                "position": "fixed",
                "bottom": 0,
                "right": 0,
                "left": 0
            });
            $(window).scroll(function(evt) {
                if ($(window).scrollTop() < $(document).height() / 2) {
                    $("footer").css("bottom", "-" + $(window).scrollTop() + "px");
                } else {
                    $("footer").css("bottom", "-" + ($(document).height() - ($(window).scrollTop() + $(window).height())) + "px");
                }
            });
            
            $("h1>span").css("width", $('h1>span').width());
            $("h1>span").each(function() {
                $(this).prop('Counter', 0).animate({
                    Counter: $(this).text().replace(" ", "")
                }, {
                    duration: 1000,
                    easing: "easeOutQuad",
                    step: function(now) {
                        $(this).html(Math.ceil(now).toString().replace(/\B(?=(\d{3})+(?!\d))/g, "&nbsp;"));
                    }
                });
            });
        });
    </script>
</body>

</html>