<!DOCTYPE html>

<html lang="fr">

<head>
    <meta charset="utf-8" />
    <title>Compteur collaboratif du référendum ADP, statistique mise à jour tous les jours : RIP, le compteur. #ReferendumADP</title>
    <meta property="og:title" content="Compteur collaboratif du référendum ADP, statistique mise à jour tous les jours : RIP, le compteur. #ReferendumADP" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://dav.li/rip-le-compteur/" />
    <meta property="og:description" content="Combien de signatures y-a-t-il sur le référendum ADP ? Compteur collaboratif des signatures pour le référendum d'initiative partagée contre la privatisation de l'aéroport de Paris aussi nommé proposition de loi référendaire visant à affirmer le caractère de service public national de l'exploitation des aérodromes de Paris. Statistique mise à jour en temps réel, tous les jours, avec les contributions de volontaires." />
    <meta property="description" content="Combien de signatures y-a-t-il sur le référendum ADP ? Compteur collaboratif des signatures pour le référendum d'initiative partagée contre la privatisation de l'aéroport de Paris aussi nommé proposition de loi référendaire visant à affirmer le caractère de service public national de l'exploitation des aérodromes de Paris. Statistique mise à jour en temps réel, tous les jours, avec les contributions de volontaires." />
    <meta property="og:locale" content="fr_FR" />
    <link rel="shortcut icon" type="image/png" href="Logo.png" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="theme-color" content="#0070C0">
    <link rel="stylesheet" href="style.css" />
    <link rel="stylesheet" href="https://dav.li/forkawesome/1.0.11/css/fork-awesome.min.css" />
    <link rel="stylesheet" href="//dav.li/jquery/ui/jquery-ui.min.css" />
</head>

<body>

    <header>
        <h1><a href="https://dav.li/rip-le-compteur"><img src="twemoji/signature.png" alt="emoji signature" /> RIP, le compteur.</a></h1>
        <div id="description"><p>Compteur collaboratif des signatures du référendum ADP</p></div>
    </header>