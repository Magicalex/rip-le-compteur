<?php

function getTotalInterval($interval){
    $days = $interval->format('%a');
    $seconds = 0;
    if($days){
        $seconds += 24 * 60 * 60 * $days;
    }
    $hours = $interval->format('%H');
    if($hours){
        $seconds += 60 * 60 * $hours;
    }
    $minutes = $interval->format('%i');
    if($minutes){
        $seconds += 60 * $minutes;
    }
    $seconds += $interval->format('%s');
    return $interval->format('%R').$seconds;
}
function array_median($array) {
  $iCount = count($array);
  if ($iCount == 0) {
    throw new DomainException('Median of an empty array is undefined');
  }
  $middle_index = floor($iCount / 2);
  sort($array, SORT_NUMERIC);
  $median = $array[$middle_index];
  return $median;
}


function getBdd($date="now"){
    $alphabet="ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    
    $totalContrib=0;
    $bdd_digramme=[];
    $bdd_single=[];
    $bdd_dated=[];

    ini_set('auto_detect_line_endings',TRUE);
    $handle = fopen("data.txt",'r');
    while ( ($data = fgetcsv($handle, 1000)) !== FALSE ) {
        if(isset($data[0]) && isset($data[3]) && strlen($data[0])==17 && sizeOf($data)<=7 && DateTime::createFromFormat('d/m/y H:i:s', $data[0])) {
            $totalContrib++;
            if($data[3]=="single"){
                $dateDiff=round(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))/3600,PHP_ROUND_HALF_DOWN);

                $thisData=[
                    "date"      => $data[0],
                    "compteur"  => intval($data[4] ?? -1),
                    "nb_pages"  => intval($data[5] ?? -1),
                    "is_last"   => boolval($data[6] ?? FALSE),
                ];

                if($bdd_dated[$dateDiff]==null){
                    $bdd_dated[$dateDiff]=[];
                }
                $bdd_dated[$dateDiff][]= $thisData;

                $bdd_single[] = $thisData;
            }else{
                if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $data[0])->diff($date))>=0){
                    $thisData=[
                        "date"      => $data[0],
                        "section"      => $data[3],
                        "compteur"  => intval($data[4] ?? -1),
                        "nb_pages"  => intval($data[5] ?? -1),
                        "is_last"   => boolval($data[6] ?? FALSE),
                    ];
                    if($bdd_digramme[$data[3]]==null){
                        $bdd_old[$data[3]]=[];
                    }
                    $bdd_digramme[$data[3]][]=$thisData;
                }
            }
        }

    }
    ini_set('auto_detect_line_endings',FALSE);
    $bdd_alph=[];
    for ($i=0; $i<26; $i++) {//par ordre alphabéthique
        for ($j=0; $j<26; $j++) {
            $section=$alphabet[$i].$alphabet[$j];
            $bdd_alph[$section]=$bdd_digramme[$section];
        }
    }
    return [
        "bdd_digramme"=>$bdd_alph,
        "bdd_single"=>$bdd_single,
        "bdd_single_dated"=>$bdd_dated,
        "totalContrib"=>$totalContrib
    ];
}



function getData($date="now",$refreshCache=false){
    $handle = fopen("cache.txt", "r");
    $cache = fread($handle, filesize("cache.txt"));
    fclose($handle);
    $cache=json_decode($cache,true);
    $now=new DateTime("now");
    if(!$refreshCache && $date=="now" && $cache!=null && getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $cache["date"])->diff($now))<=60){
        return $cache;
    }else{//refresh cache and provide data
        if($date=="now"){
            $date=new DateTime("now");
        }else{
            $date=DateTime::createFromFormat('d/m/y H:i:s', $date);
        }
        
        $getBdd=getBdd($date);
        $bdd_digramme=$getBdd["bdd_digramme"];
        $bdd_single=$getBdd["bdd_single"];
        $bdd_single_dated=$getBdd["bdd_single_dated"];
        $totalContrib=$getBdd["totalContrib"];
        
        //calcul du total single
        $h=-1;
        $total_single=-1;
        $precisOnly=true;
        while($total_single==-1){
            $h++;
            
            $contribs=$bdd_single_dated[$h];
            
            if($contribs){
                if($precisOnly){
                    $contribs=array_filter($contribs,function($value) {
                        return $value["is_last"];
                    });
                }
                if(sizeof($contribs)>=2){
                    $compteurs=array_column($contribs, "compteur");
                    $pages=array_column($contribs, "nb_pages");
                    $total_single=end($compteurs);
                    $totalContradiction=array_count_values($compteurs);
                    $page=end($pages);
                }   
            }
            if($h>=72){
                if($precisOnly==false){
                    $total_single=0;
                }else{
                    $h=0;
                    $precisOnly=false;
                }
            }
        }
        
        //méthode digramme
        $bdd_last=[];
        $bdd_best=[];
        foreach ($bdd_digramme as $section => $lignes) {
            $bdd_last[$section] = end($lignes);
            // On parcourt en partant de la fin
            for ($k = count($lignes) - 1; $k >= 0; $k--) {
                if (DateTime::createFromFormat('d/m/y H:i:s', $lignes[$k]['date'])->diff($date)->format("%a")<=1 && $lignes[$k]['is_last']) {
                    $bdd_best[$section] = $lignes[$k];
                    break;
                }
            }
            if (!isset($bdd_best[$section])) {
                // On a pas trouvé de compte exact, on prend le dernier
                $bdd_best[$section] = $bdd_last[$section];
            }
            $total_digramme+=$bdd_best[$section]["compteur"];
        }
        
        $total=max($total_digramme,$total_single);
        
        if(getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', "25/06/19 23:59:59")->diff($date))>0){
            $difference=378477-373772; //différence observée le 25/06
            $pourcentage_nonaffichees=$difference/373772; //calcul du pourcentage de signatures non-affichées (c.f. FAQ)
            $signatures_nonaffichees=round($total*$pourcentage_nonaffichees);
            $total_estimation=round($total*(1+$pourcentage_nonaffichees),-3,PHP_ROUND_HALF_DOWN);
            $total=$total_estimation;
        }
        
        $pourcentage=round($total/4717396*100,2);
        $precis=$precisOnly;
        $data=[
            "date"=>date("d\/m\/y H:i:s"),
            "total"=>$total,
            "total_digramme"=>$total_digramme,
            "total_estimation"=>$total_estimation,
            "estimation_signatures_nonaffichees"=>$signatures_nonaffichees,
            "total_single"=>$total_single,
            "pourcentage"=>$pourcentage,
            "precis"=>$precis,
            "page_single"=>$page,
            "hoursAgo_single"=>$h,
            "contribsCount_single"=>sizeof($contribs),
            "totalContradiction_single"=>$totalContradiction,
            "bdd_dated"=>$bdd_dated,
            "bdd_calculation"=>$contribs,
            "bdd_best"=>$bdd_best,
            "bdd_last"=>$bdd_last,
            "totalContrib"=>$totalContrib
        ];
        $handle = fopen("cache.txt", "w");
        fwrite($handle, json_encode($data));
        fclose($handle);
        return $data;
    }
}


function getStats(){
    $handle = fopen("cache-stats.txt", "r");
    $cache = fread($handle, filesize("cache.txt"));
    fclose($handle);
    $cache=json_decode($cache,true);
    
    $now=new DateTime("now");
    $dateDebut=DateTime::createFromFormat('d/m/y H:i:s', "18/06/19 23:00:00");
    $days=$dateDebut->diff($now)->format("%a")+1;
    
    if(sizeof($cache["stats"])==$days){
        $stats=$cache["stats"];
    }else{
        $stats=[];
        $dateStat=$dateDebut->modify("-1 day");
        for($d=0;$d<$days;$d++){
            $dateStat->modify("+1 day");
            $getData=getData(date_format($dateStat,"d\/m\/y H:i:s"));
            $localStat=[
                "date"=>date_format($dateStat,"d\/m\/y H:i:s"),
                "total"=>$getData["total"],
                "pourcentage"=>$getData["pourcentage"],
            ];
            $stats[$d]=$localStat;
        }
        $data=[
            "date"=>date("d\/m\/y H:i:s"),
            "stats"=>$stats,
        ];
        $handle = fopen("cache-stats.txt", "w");
        fwrite($handle, json_encode($data));
        fclose($handle);
    }
    return($stats);
}