<?php include("func.php");

$getData=getData();
$total=$getData["total"];
$total_digramme=$getData["total_digramme"];
$estimation_signatures_nonaffichees=$getData["estimation_signatures_nonaffichees"];
$pourcentage=$getData["pourcentage"];
$precis=$getData["precis"];

$bdd_dated=$getData["bdd_dated"];
$bdd_best=$getData["bdd_best"];
$bdd_last=$getData["bdd_last"];
$totalContrib=$getData["totalContrib"];

$stats=getStats();



$now=new DateTime("now");

$bdd_maj_priorite=[];

$maximum=null;

$table_popularite="";
$bdd_sorted = $bdd_best;
uasort($bdd_sorted, function($a, $b) {
    return $b['compteur'] <=> $a['compteur'];
});
foreach ($bdd_sorted as $s => $donnees) {
    $table_popularite.='<tr><th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><th class="bar" style="width:'.($donnees['compteur']/10).'px">'.$donnees['compteur'].'</th></tr>';
    if($maximum==null){
        $maximum=$donnees["compteur"];
    }
}

$total_a_maj=0;
$table_maj="";
$bdd_sorted = $bdd_last;
uasort($bdd_sorted, function($a, $b) {
    return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
});
$date10h=$now;
$date10h=$date10h->setTime(10,0,0);
foreach ($bdd_sorted as $s => $donnees) {
    $opacity='';
    if(DateTime::createFromFormat('d/m/y H:i:s', $donnees['date'])->diff($now)->format("%a")>1 || getTotalInterval(DateTime::createFromFormat('d/m/y H:i:s', $donnees['date'])->diff($date10h))<0){
        $opacity=' style="opacity:0.5" ';
    }else{
        $total_a_maj++;
        if($donnees["compteur"]>=5){
            $bdd_maj_priorite[]=$s;
        }
    }
    $table_maj.='<tr><th'.$opacity.'><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$donnees["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><th'.$opacity.'><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$donnees["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$donnees['date'].'</a> <span class="badge">'.$bdd_best[$s]["compteur"].'</span></th></tr>';
}

$preciser=0;
$table_preciser="";
$bdd_sorted = $bdd_best;
uasort($bdd_sorted, function($a, $b) {
    return DateTime::createFromFormat('d/m/y H:i:s', $a['date']) <=> DateTime::createFromFormat('d/m/y H:i:s', $b['date']);
});
foreach ($bdd_sorted as $s => $donnees) {
    if ($donnees['compteur'] > 200 && !$donnees['is_last']) {
        $bdd_maj_priorite[]=$s;
        $preciser++;
        $table_preciser.='<tr><th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$s.'</a></th><th><a href="https://www.referendum.interieur.gouv.fr/consultation_publique/8/'.$s[0].'/'.$s.'?page='.$bdd_last[$s]["nb_pages"].'" target="_blank" rel="noopener noreferrer">'.$donnees['date'].' n\'est pas précis</a> <span class="badge">'.$donnees["compteur"].'</span></th></tr>';
    }
}