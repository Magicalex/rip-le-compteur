console.log("RIP, le compteur.");
if ($("#formulaire_consultation_publique>.well:nth-of-type(4)>:first-child").prop("tagName") == "TABLE" || $("#formulaire_consultation_publique>.well:nth-of-type(4) h2").text()=="Aucun soutien n'est accepté jusqu'à ce jour") {
    var urlpath = location.pathname.split("/")
    var section;
    if(urlpath[3].length==1 && urlpath[4].length==1){
        section="single";
    }else if(urlpath[4].length==2){
        section=urlpath[4];
    }
    if(section==undefined){
        console.error("RIP, le compteur : section undefined");
    }else{
        var compteur=$("#formulaire_consultation_publique>.well:nth-of-type(4) tbody>tr").length;
        var nb_pages = 1;
        var is_last = 1;
        if($(".pagination").length!=0){
            if ($(".pagination .last a").length || compteur==0) { //évaluation du nombre de soutien sur la base du nb de pages / compteur==0 pour le cas où on est bien trop loin
                nb_pages = parseInt($(".pagination .last a").attr("href").split("=")[1]);
                compteur = nb_pages*200-100;
                is_last = 0;
            }else{ //Cas de la consultation d'une dernière page
                nb_pages = parseInt($(".pagination .current").text());
                compteur = (nb_pages - 1) * 200 + $("#formulaire_consultation_publique>.well:nth-of-type(4) tbody>tr").length;
            }
        }
        if($("#formulaire_consultation_publique>.well:nth-of-type(4) h2").text()=="Aucun soutien n'est accepté jusqu'à ce jour"){
            compteur=0;
        }
        console.log(compteur);
        $.post("http://dav.li/rip-le-compteur/depot.php", {
                consultationId: urlpath[2],
                section: section,
                compteur: compteur,
                nb_pages: nb_pages,
                is_last: is_last
            },
            function (returnedData) {
                console.log(returnedData);
            }).fail(function (error) {
                console.log(error);
            });
    }
}